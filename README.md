# MemoJS-Base-project

Un projet vide préconfiguré pour créer des contenus avec MemoJS.

# Usage

**Prérequis**

Node.js (version stable) doit être installé sur votre système : [Télecharger Node.js](https://nodejs.org/en/download/)

**Les commandes ci-dessous doivent être lancées depuis un terminal**

- Cloner le répertoire où vous le souhaitez dans votre système local :

> cd /where/you/want/to/clone

> git clone https://gitlab.com/esad-gv1/memojs-base-project.git

- Entrer dans le répertoire local

> cd memojs-base-project

NB : vous pouvez renommer le dossier `memojs-base-project` librement! Mais il faudra bien utiliser ce nouveau nom dans les lignes de commandes.

- Installer les packages npm et les dépendances nécessaires au bon fonctionnement du projet :

> npm install

- Tester le système de compilation pour la distribution :

> npm run build

Cette commande utilise `Webpack` pour assembler les fichiers et les transpiler via `Babel` depuis ES6 vers une version universelle de JavaScript. Le résultat est un ensemble de fichiers contenu dans le dossier `dist`. C'est le contenu de ce dossier qui peut être mis en ligne sur un serveur web.

- Rendre automatique la recompilation à chaque modification des sources (contenu du dossier `src`):

> npm run watch

Cette commande évite de refaire soi-même une compilation manuellement : à chaque sauvegarde d'un fichier suite à une modification, la compilation est refaite automatiquement. Idéal pour tester localement et rapidement ce qui est en cours d'écriture.

- Tester localement votre travail :

> npm run start

Un serveur local est créé et sert le contenu du dossier `dist`. Il est donc impératif de faire une compilation au préalable. Pour y accéder, utiliser son navigateur préféré et utiliser l'URL : http://127.0.0.1:8080.

## Comment ajouter un épisode

!! Pour des exemples complets, se référer à !!

Il suffit d'ajouter un fichier `.md` dans le dossier `src/md` et d'y écrire l'épisode en question en respectant le format Markdown.

Dans le fichier index.md, il faut ajouter un lien dans la liste des liens :

```
[Convertir un texte en span](?mdpath=md/text2span.md)
```

L'URL du lien doit impérativement être relative et transmettre un paramètre `?mdpath=md/pathToYourFile.md` où pathToYourFile doit être remplacé par le nom de votre nouveau fichier.

### Utilisations de ressources et médias

Le dossier ```src/assets``` est prévu pour accueillir vos ressources. Les liens pourront êtres insérés ainsi dans les épisodes (ici pour un fichier svg avec application de styles) :

```
![](assets/img/variable.svg){style="width: 30%; height: auto"}
````

## Structure de l'outil et fonctionnalités

Le langage [MarkDown](https://www.markdownguide.org/basic-syntax/) est utilisé pour créer les contenus à afficher. Il faut donc respecter la syntaxe de ce langage pour que les documents soient bien interprétés.

Quelques fonctionnalités "maison" ont été ajoutées spécifiquement pour cet outil. Il s'agit parfois d'extensions au MarkDown déjà disponibles à travers la librairie de conversion de MarkDown en HTML qui est ici utilisé ([markdown-it](https://github.com/markdown-it/markdown-it) et [markdown-it-attrs.browser](https://github.com/arve0/markdown-it-attrs)).

- Possibilité de définir une destination pour les liens :

```
[mon lien](url_du_lien){target=blank}
```

Les valeurs habituelles de l'attribut `target` des éléments `<a>` peuvent être utilisés.

- Possibilité de définir un style particulier pour un paragraphe :

```
Ma phrase.{style="opacity:0"}
```

Ici, le paragraphe "Ma phrase." sera totalement transparent.

- Possibilité d'insérer une iFrame exécutant le code source présenté :

  Un parser complémentaire a été ajouté à `highlight.js`, librairie qui permet de générer un affichage stylisé des codes sources en HTML en associant automatique une feuille de style de colorisation des mots clés, langage par langage.
  En ajoutant le préfix `render-<lettre>` au code source que vous voulez faire afficher dans une iFrame d'exécution, différentes sources peuvent être associées entre elles.

```

```
